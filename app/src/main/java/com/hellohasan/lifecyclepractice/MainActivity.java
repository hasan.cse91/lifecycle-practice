package com.hellohasan.lifecyclepractice;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView tapCounterTextView;
    private TextView locationTextView;
    private SharedPreferences sharedPreferences;
    private String location;
    private int counter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = getApplicationContext().getSharedPreferences("my_preference", Context.MODE_PRIVATE);
        tapCounterTextView = findViewById(R.id.tap_counter);
        locationTextView = findViewById(R.id.locationTextView);

        counter = 0;
    }

    @Override
    protected void onResume() {
        super.onResume();

        location = sharedPreferences.getString("location", "Dhaka");
        locationTextView.setText(getString(R.string.location, location));
        tapCounterTextView.setText(getString(R.string.tap_count, counter));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void count(View view) {
        counter++;
        tapCounterTextView.setText(getString(R.string.tap_count, counter));
    }
}
