package com.hellohasan.lifecyclepractice;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class SettingsActivity extends AppCompatActivity {

    private TextView tapCounterTextView;
    private EditText locationEditText;
    private int counter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        counter = 0;
        locationEditText = findViewById(R.id.locationEditText);
        tapCounterTextView = findViewById(R.id.tap_counter);

        if(savedInstanceState!=null)
            counter = savedInstanceState.getInt("counter");

        tapCounterTextView.setText(getString(R.string.tap_count, counter));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("counter", counter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId()==android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    public void saveData(View view) {

        SharedPreferences preferences = getApplicationContext().getSharedPreferences("my_preference", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("location", locationEditText.getText().toString());
        editor.apply();

        finish();
    }

    public void count(View view) {
        counter++;
        tapCounterTextView.setText(getString(R.string.tap_count, counter));
    }
}
